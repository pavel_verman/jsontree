import JsonTree.Exception.JsonTreeException;
import JsonTree.JsonTree;
import JsonTree.JsonTreeStatus;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SimpleJsonTreeTest {
    @Test
    public void testJoinEqualsTrees() {
        String exp = " {\"name\":\"root\", \"children\":[{\"name\":\"a\"}, {\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";

        JsonTree my_json_tree1 = new JsonTree();
        JsonTreeStatus load_status1 = my_json_tree1.loadTree(exp);
        JsonTree my_json_tree2 = new JsonTree();
        JsonTreeStatus load_status2 = my_json_tree2.loadTree(exp);
        assertTrue(0 == load_status1.getCode());
        assertTrue(0 == load_status2.getCode());
        try {
            assertEquals(my_json_tree1, my_json_tree1.joinTrees(my_json_tree2));
        } catch (JsonTreeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testJoinEmptyTree() {
        String exp1 = " {\"name\":\"root\", \"children\":[{\"name\":\"a\"}, {\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";
        String exp2 = " {\"name\":\"root\"}";
        String expected_result = "{\"children\":[{\"name\":\"a\",\"label\":\"first\"}," +
                "{\"children\":[{\"name\":\"c\",\"label\":\"first\"}," +
                "{\"name\":\"d\",\"label\":\"first\"}],\"name\":\"b\",\"label\":\"first\"}]," +
                "\"name\":\"root\",\"label\":\"children\"}";
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(expected_result);
            JSONObject expected_json_obj = (JSONObject) obj;

            JsonTree my_json_tree1 = new JsonTree();
            JsonTreeStatus load_status1 = my_json_tree1.loadTree(exp1);
            JsonTree my_json_tree2 = new JsonTree();
            JsonTreeStatus load_status2 = my_json_tree2.loadTree(exp2);
            assertTrue(0 == load_status1.getCode());
            assertTrue(0 == load_status2.getCode());
            try {
                assertEquals(expected_json_obj, my_json_tree1.joinTrees(my_json_tree2).getJSONObject());
            } catch (JsonTreeException e) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = JsonTreeException.class)
    public void testJoinSameTreesWithDifferentRoots() throws JsonTreeException {
        String exp1 = " {\"name\":\"root1\", \"children\":[{\"name\":\"a\"}, {\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";
        String exp2 = " {\"name\":\"root2\", \"children\":[{\"name\":\"a\"}, {\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";

        JsonTree my_json_tree1 = new JsonTree();
        JsonTreeStatus load_status1 = my_json_tree1.loadTree(exp1);
        JsonTree my_json_tree2 = new JsonTree();
        JsonTreeStatus load_status2 = my_json_tree2.loadTree(exp2);
        assertTrue(0 == load_status1.getCode());
        assertTrue(0 == load_status2.getCode());
        my_json_tree1.joinTrees(my_json_tree2);
        //assertTrue(false);
    }

    @Test
    public void testJoinComplexCaseTrees() {
        String exp1 = " {\"name\":\"root\", \"children\":[{\"name\":\"a\"}, " +
                "{\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";
        String exp2 = " {\"name\":\"root\", \"children\":[{\"name\":\"a\", \"children\":[{\"name\":\"ab\"}]}, " +
                "{\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"db\"}]}] }";
        String expected_result = "{\"children\":[{\"children\":[{\"name\":\"ab\",\"label\":\"second\"}]," +
                "\"name\":\"a\",\"label\":\"children\"},{\"children\":[{\"name\":\"c\"}," +
                "{\"name\":\"db\",\"label\":\"second\"}," +
                "{\"name\":\"d\",\"label\":\"first\"}],\"name\":\"b\",\"label\":\"children\"}]," +
                "\"name\":\"root\"}";
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(expected_result);
            JSONObject expected_json_obj = (JSONObject) obj;

            JsonTree my_json_tree1 = new JsonTree();
            JsonTreeStatus load_status1 = my_json_tree1.loadTree(exp1);
            JsonTree my_json_tree2 = new JsonTree();
            JsonTreeStatus load_status2 = my_json_tree2.loadTree(exp2);
            assertTrue(0 == load_status1.getCode());
            assertTrue(0 == load_status2.getCode());
            try {
                assertEquals(expected_json_obj, my_json_tree1.joinTrees(my_json_tree2).getJSONObject());
            } catch (JsonTreeException e) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testJoinSubTrees() throws JsonTreeException {
        String exp1 = " {\"name\":\"root\", \"children\":[{\"name\":\"b\",\"children\":[{\"name\":\"d\"}]}] }";
        String exp2 = " {\"name\":\"root\", \"children\":[{\"name\":\"a\"}, {\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";
        String expected_result = "{\"children\":[{\"name\":\"a\",\"label\":\"second\"}," +
                "{\"children\":[{\"name\":\"c\",\"label\":\"second\"},{\"name\":\"d\"}]," +
                "\"name\":\"b\",\"label\":\"children\"}]," +
                "\"name\":\"root\",\"label\":\"children\"}";
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(expected_result);
            JSONObject expected_json_obj = (JSONObject) obj;

            JsonTree my_json_tree1 = new JsonTree();
            JsonTreeStatus load_status1 = my_json_tree1.loadTree(exp1);
            JsonTree my_json_tree2 = new JsonTree();
            JsonTreeStatus load_status2 = my_json_tree2.loadTree(exp2);
            assertTrue(0 == load_status1.getCode());
            assertTrue(0 == load_status2.getCode());
            try {
                assertEquals(expected_json_obj, my_json_tree1.joinTrees(my_json_tree2).getJSONObject());
            } catch (JsonTreeException e) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
