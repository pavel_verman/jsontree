import JsonTree.*;
import JsonTree.Exception.JsonTreeException;

public class Main {
    public static void main(String[] args) {
        String exp1 = " {\"name\":\"root\", \"children\":[{\"name\":\"a\"}, {\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";
        String exp2 = " {\"name\":\"root\", \"children\":[{\"name\":\"a\"}, {\"name\":\"b\",\"children\":[{\"name\":\"c\"},{\"name\":\"d\"}]}] }";

        JsonTree my_json_tree = new JsonTree();
        JsonTreeStatus load_status = my_json_tree.loadTree(exp1);
        JsonTree my_json_tree2 = new JsonTree();
        JsonTreeStatus load_status2 = my_json_tree2.loadTree(exp2);
        System.out.println(load_status.getDescription());
        System.out.println(load_status2.getDescription());
        try {
            System.out.println(my_json_tree.joinTrees(my_json_tree2));
            System.out.println(my_json_tree);
        } catch (JsonTreeException e) {
            e.printStackTrace();
        }
    }
}