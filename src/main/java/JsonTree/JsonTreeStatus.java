package JsonTree;

public enum JsonTreeStatus {
    DATA_LOADED(0, "It's ok, json tree is successfully loaded"),
    NOT_JSON_FORMAT(1, "Looks like given string is not json format, error during parsing this string"),
    INCORRECT_STRUCTURE(2, "Given json string have incorrect format, see examples"),
    DIFFERENT_ROOTS(3, "Names of the tree roots must be same"),
    NULL_POINTER(4, "Need to load json tree data first");

    private int code;
    private String description;

    private JsonTreeStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {return description;}
    public int getCode() {return code;}
};