/*
 * Copyright (C) 2018 JmanJ Company
 */

package JsonTree;

import JsonTree.Exception.JsonTreeException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashSet;

/**
 * Stores information about the tree specified in json format
 */
public class JsonTree {
    private JSONObject tree_struct;

    // constructor for internal use
    private JsonTree(JSONObject json_obj){
        tree_struct = json_obj;
    }

    public JsonTree(){
        this.tree_struct = null;
    }

    /**
     * Loads the tree structure
     * Structure must have one root element
     * All nodes must have field "name" with type String
     * A Node may contain field "children" with type List (elements are nodes)
     */
    public JsonTreeStatus loadTree(String jsonString){
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(jsonString);
            JSONObject json_obj = (JSONObject) obj;
            if (isValidJsonStructure(json_obj)){
                tree_struct = json_obj;
                return JsonTreeStatus.DATA_LOADED;
            }
            else{
                return JsonTreeStatus.INCORRECT_STRUCTURE;
            }
        } catch (ParseException e) {
            return JsonTreeStatus.NOT_JSON_FORMAT;
        }
    }

    /**
     * Joins two trees according to some rules.
     * Matched nodes are merge into one.
     * A field "label" is added to the node in resulted tree:
     * 1. value = "first" (if the node is only in the first tree)
     * 2. value = "second" (if the node is only in the second tree)
     * 3. value = "children" (if the node can not be labeled first or second and has unmatched children)
     */
    public JsonTree joinTrees(JsonTree joined_tree) throws JsonTreeException {
        JSONObject joined_tree_struct = joined_tree.getJSONObject();
        if ((tree_struct == null)||(joined_tree_struct == null))
            throw new JsonTreeException(JsonTreeStatus.NULL_POINTER);
        JSONObject new_tree_struct = process_layer(tree_struct, joined_tree_struct);
        return new JsonTree(new_tree_struct);
    }

    private JSONObject process_layer(JSONObject layer_one, JSONObject layer_two) throws JsonTreeException {
        String node_name1 = (String) layer_one.get("name");
        String node_name2 = (String) layer_two.get("name");
        if (!node_name1.equals(node_name2)){
            throw new JsonTreeException(JsonTreeStatus.DIFFERENT_ROOTS);
        }
        HashSet<String> existing_first_layer_names = new HashSet<>();
        JSONArray json_layer_one_array = (JSONArray) layer_one.get("children");
        JSONArray json_layer_two_array = (JSONArray) layer_two.get("children");
        JSONObject result_tree = new JSONObject();
        JSONArray result_children_array = new JSONArray();
        boolean has_mismatched_children = false;

        if (json_layer_one_array != null) {
            for (Object layer_one_node : json_layer_one_array) {
                JSONObject json_layer_one_node = (JSONObject) layer_one_node;
                existing_first_layer_names.add((String) json_layer_one_node.get("name"));
            }
        }
        if (json_layer_two_array != null) {
            for (Object layer_two_node : json_layer_two_array) {
                JSONObject json_layer_two_node = (JSONObject) layer_two_node;
                String second_node_name = (String) json_layer_two_node.get("name");
                if (existing_first_layer_names.contains(second_node_name)) {
                    // nodes with same names founded
                    existing_first_layer_names.remove(second_node_name);
                    for (Object layer_one_node : json_layer_one_array) {
                        // find this node in first again
                        JSONObject json_layer_one_node = (JSONObject) layer_one_node;
                        String first_node_name = (String) json_layer_one_node.get("name");
                        if (second_node_name.equals(first_node_name)) {
                            JSONObject common_branch = process_layer(json_layer_one_node, json_layer_two_node);
                            result_children_array.add(common_branch);
                            break;
                        }
                    }
                } else {
                    // construct second branches
                    JSONObject branch_second_node = constructLabeledBranch(json_layer_two_node, "second");
                    result_children_array.add(branch_second_node);
                    has_mismatched_children = true;
                }
            }
        }

        // check that someone node must be first
        if (existing_first_layer_names.size() > 0){
            has_mismatched_children = true;
            // now to construct first branches
            for (Object layer_one_node: json_layer_one_array) {
                JSONObject json_layer_one_node = (JSONObject) layer_one_node;
                String first_node_name = (String) json_layer_one_node.get("name");
                if (existing_first_layer_names.contains(first_node_name)){
                    JSONObject branch_first_node = constructLabeledBranch(json_layer_one_node, "first");
                    result_children_array.add(branch_first_node);
                }
            }
        }
        result_tree.put("name", node_name1);
        if (has_mismatched_children){
            result_tree.put("label", "children");
        }
        if (result_children_array.size() > 0) {
            result_tree.put("children", result_children_array);
        }
        return result_tree;
    }

    private JSONObject constructLabeledBranch(JSONObject layer, String branch_label){
        JSONObject result_branch = new JSONObject();
        result_branch.put("name", layer.get("name"));
        result_branch.put("label", branch_label);
        if (layer.containsKey("children")){
            JSONArray childrens_array = (JSONArray) layer.get("children");
            JSONArray result_children_array = new JSONArray();
            for (Object children_obj: childrens_array) {
                result_children_array.add(constructLabeledBranch((JSONObject) children_obj, branch_label));
            }
            result_branch.put("children", result_children_array);
        }
        return result_branch;
    }

    private boolean isValidJsonStructure(JSONObject json_obj){
        //System.out.println(json_obj);
        if (!json_obj.containsKey("name")){
            // node must have attribute "name"
            return false;
        }
        else{
            if (!(json_obj.get("name") instanceof String))
                // and node name must be String type
                return false;
            if (json_obj.containsKey("children")){
                // check all child nodes
                JSONArray json_sub_array = (JSONArray) json_obj.get("children");
                boolean sub_array_is_correct = true;
                for (Object json_sub_obj: json_sub_array) {
                    if (!isValidJsonStructure((JSONObject) json_sub_obj)) {
                        sub_array_is_correct = false;
                        break;
                    }
                }
                return sub_array_is_correct;
            }
            return true;
        }
    }

    public JSONObject getJSONObject(){
        return tree_struct;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof JsonTree && this.tree_struct.equals(((JsonTree) obj).tree_struct);
    }

    @Override
    public String toString() {
        return tree_struct.toString();
    }
}
