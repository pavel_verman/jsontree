package JsonTree.Exception;

import JsonTree.JsonTreeStatus;

public class JsonTreeException extends Exception{
    public JsonTreeException(JsonTreeStatus exc_status) {
        super(exc_status.getDescription());
    }
}
